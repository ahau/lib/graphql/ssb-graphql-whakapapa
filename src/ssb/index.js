const GetLinks = require('./queries/get-links')
const GetWhakapapaLink = require('./queries/get-whakapapa-link')
const LoadFamilyOfPerson = require('./queries/load-family-of-person')

const TombstoneProfileAndLinks = require('./mutations/tombstone-profile-and-links')

const LinksCache = require('./links-cache')
const LinkCache = require('./link-cache')

module.exports = function (ssb, externalGetters) {
  const { getProfile } = externalGetters

  const linksCache = LinksCache(ssb) // stores collections of links to / from profiles (older)
  const getLinks = GetLinks(ssb, linksCache)

  const linkCache = LinkCache(ssb) // stores individual links (newer)

  return {
    getLinks,
    getWhakapapaLink: GetWhakapapaLink(ssb, { getLinks, getProfile }),
    loadFamilyOfPerson: LoadFamilyOfPerson(getLinks),

    tombstoneProfileAndLinks: TombstoneProfileAndLinks(ssb),

    gettersWithCache: {
      getChildLink (id, cb) {
        id = typeof id === 'object' ? id.key : id

        const link = linkCache.get(id)
        if (link) return cb(null, link)

        ssb.whakapapa.child.get(id, (err, link) => {
          if (err) return cb(null, null) // swallows error

          linkCache.set(id, link)
          cb(null, link)
        })
      },
      getPartnerLink (id, cb) {
        id = typeof id === 'object' ? id.key : id

        const link = linkCache.get(id)
        if (link) return cb(null, link)

        ssb.whakapapa.partner.get(id, (err, link) => {
          if (err) return cb(null, null) // swallows error

          linkCache.set(id, link)
          cb(null, link)
        })
      }
    }
  }
}

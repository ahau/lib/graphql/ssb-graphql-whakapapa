const Cache = require('hashlru')

module.exports = function linkCache (sbot) {
  const cache = Cache(10e3) // 10,000

  const API = {
    set (linkId, link) {
      link.states = null // prune of un-needed data
      // NOTE delete link.states is MUCH slower
      cache.set(linkId, link)
    },
    has: cache.has,
    get: cache.get
    // remove: cache.remove
  }

  // invalidate cache if any new message comes in which could be a update
  // for a link we have cached

  sbot.post(m => {
    sbot.get({ id: m.key, meta: true, private: true }, (err, m) => {
      if (err) return

      if (typeof m.value.content === 'string') return

      const root = m.value.content?.tangles?.link?.root
      if (!root) return

      if (cache.has(root)) cache.remove(root)
    })
  })

  return API
}

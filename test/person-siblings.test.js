const tape = require('tape')
const TestBot = require('./test-bot')
const { SaveProfile, SaveLink, GetProfile } = require('./lib/helpers')

tape('person.siblings (saveLink + person)', async t => {
  const { ssb, apollo } = await TestBot()

  const savePerson = SaveProfile(apollo, t)
  const saveLink = SaveLink(apollo, t)
  const getPerson = GetProfile(apollo, t)

  const defaultAttrs = {
    type: 'person',
    authors: {
      add: ['*']
    }
  }

  const mumId = await savePerson({
    ...defaultAttrs,
    preferredName: 'Claudine'
  })

  const dadId = await savePerson({
    ...defaultAttrs,
    preferredName: 'Stacey'
  })

  const children = ['Zara', 'Cherese', 'Peaches', 'Damon']

  // save profiles for all of the children
  const childIds = await Promise.all(
    children.map(childName => savePerson({ ...defaultAttrs, preferredName: childName }))
  )

  // save links between mum and the children
  await Promise.all(
    childIds.map(id => saveLink({
      type: 'link/profile-profile/child',
      parent: mumId,
      child: id
    }))
  )

  // save links between dad and the childrens
  await Promise.all(
    childIds.map(id => saveLink({
      type: 'link/profile-profile/child',
      parent: dadId,
      child: id
    }))
  )

  const otherChildId = await savePerson({
    ...defaultAttrs,
    preferredName: 'Zavien'
  })

  // create the link between the parents and the child
  await saveLink({
    type: 'link/profile-profile/child',
    parent: dadId,
    child: otherChildId
  })

  // get the dads  profile and check the partners
  const otherChildProfile = await getPerson(otherChildId)

  t.deepEqual(
    otherChildProfile.siblings.sort((a, b) => a.id > b.id),
    childIds.map(id => ({ id })).sort((a, b) => a.id > b.id),
    'returns all siblings'
  )

  ssb.close()
  t.end()
})

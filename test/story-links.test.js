const tape = require('tape')
const { promisify: p } = require('util')
const TestBot = require('./test-bot')
const { isMsgId } = require('ssb-ref')
const {
  SaveStory,
  SaveArtefact,
  SaveProfile,
  SaveLink,
  GetStory,
  GetArtefact,
  GetProfile
} = require('./lib/helpers')

const blobId = '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256'
const unbox = 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'

const blob = {
  type: 'ssb',
  blobId,
  mimeType: 'audio/mp3',
  size: 1212123,
  unbox
}

tape('story.*Links (saveLink, getStory) + artefact.* + person.*', async (t) => {
  t.plan(34)

  const { ssb, apollo } = await TestBot({ loadContext: true })

  const saveStory = SaveStory(apollo, t)
  const saveArtefact = SaveArtefact(apollo, t)
  const saveProfile = SaveProfile(apollo, t)

  const saveLink = SaveLink(apollo, t)
  const getStory = GetStory(apollo, t)
  const getArtefact = GetArtefact(apollo, t)
  const getProfile = GetProfile(apollo, t)

  const { groupId } = await p(ssb.tribes.create)({})
  const recps = [groupId]

  /* NOTE - these api's are primarily tested elesewhere */
  const storyId1 = await saveStory({ type: 'test', title: 'Test Story', recps })
  const storyId2 = await saveStory({ type: 'test', title: 'Test Story 2', recps })
  const artefactId = await saveArtefact({ type: 'photo', title: 'Test Artefact', blob, recps })
  const profileId = await saveProfile({ type: 'person', preferredName: 'Test Person', recps, authors: { add: [ssb.id] } })

  /* APIs we're testing for this module */

  // SAVE A LINK FOR A STORY ON A STORY
  const storyLinkId = await saveLink({ type: 'link/story-story', parent: storyId1, child: storyId2, recps })

  // SAVE A LINK FOR AN ARTEFACT ON A STORY
  const artefactLinkId = await saveLink({ type: 'link/story-artefact', parent: storyId1, child: artefactId, recps })

  // SAVE A LINK FOR A CONTRIBUTOR ON A STORY
  const contributorLinkId = await saveLink({ type: 'link/story-profile/contributor', parent: storyId1, child: profileId, recps })

  // SAVE A LINK FOR A MENTION ON A STORY
  const mentionLinkId = await saveLink({ type: 'link/story-profile/mention', parent: storyId1, child: profileId, recps })

  // SAVE A LINK FOR A CREATOR ON A STORY
  const creatorLinkId = await saveLink({ type: 'link/story-profile/creator', parent: storyId1, child: profileId, recps })

  // QUERY THE STORY AND GET THE PROFILE AS A MENTION
  const story = await getStory(storyId1)

  t.true(
    typeof story === 'object',
    'returns story object'
  )

  t.true(
    isMsgId(story.id),
    'returns storyId'
  )

  t.true(
    isMsgId(story.artefactLinks[0].linkId) && story.artefactLinks[0].linkId === artefactLinkId,
    'returns linkId for artefacts'
  )

  t.true(
    isMsgId(story.mentionLinks[0].linkId) && story.mentionLinks[0].linkId === mentionLinkId,
    'returns linkId for mentions'
  )

  t.true(
    isMsgId(story.contributorLinks[0].linkId) && story.contributorLinks[0].linkId === contributorLinkId,
    'returns linkId for contributors'
  )

  t.true(
    isMsgId(story.creatorLinks[0].linkId) && story.creatorLinks[0].linkId === creatorLinkId,
    'returns linkId for mentions'
  )

  t.true(
    isMsgId(story.storyLinks[0].linkId) && story.storyLinks[0].linkId === storyLinkId,
    'returns linkId for stories'
  )

  t.true(story.storyLinks[0].story.id === storyId2, 'storyLink story id matches linked story id')

  // QUERY THE PROFILE TO SEE LINKS
  // TODO 2022-02-17 mix - consolidate all the profile-link tests
  // this test repeats some of person-mention-links
  const profile = await getProfile(profileId)

  t.true(
    typeof profile === 'object',
    'returns profile object'
  )

  var mention = profile.mentionLinks[0]
  var contribution = profile.contributorLinks[0]
  var creation = profile.creatorLinks[0]

  t.true(
    isMsgId(mention.linkId) && mention.linkId === mentionLinkId,
    'profile mentions returns linkId and matches original'
  )

  t.true(mention.story.id === storyId1, 'returns id of original story')

  t.true(
    isMsgId(contribution.linkId) && contribution.linkId === contributorLinkId,
    'profile contributions returns linkId and matches original'
  )

  t.true(contribution.story.id === storyId1, 'returns id of origin story')

  t.true(
    isMsgId(creation.linkId) && creation.linkId === creatorLinkId,
    'profile creator returns linkId and matches original'
  )

  t.true(creation.story.id === storyId1, 'returns id of origin story')

  // QUERY THE ARTEFACT TO SEE THE LINKS
  // TODO 2022-02-17 mix - pull this into a artefact-link test
  const artefact = await getArtefact(artefactId)

  t.true(typeof artefact === 'object', 'returns artefact object')

  var artefactStory = artefact.storyLinks[0]

  t.true(
    isMsgId(artefactStory.linkId) && artefactStory.linkId === artefactLinkId,
    'profile mentions returns linkId and matches original'
  )

  t.true(artefactStory.story.id === storyId1, 'returns id of original storys')

  // TOMBSTONE A LINK
  const tombstonedLinkId = await saveLink({
    linkId: artefactLinkId,
    type: 'link/story-artefact',
    tombstone: {
      date: new Date()
    }
  })

  t.true(tombstonedLinkId === artefactLinkId, 'returns the linkId given')

  // QUERYING THE STORY AGAIN SHOULDNT RETURN THE CONTRIBUTOR
  const newStory = await getStory(storyId1)

  t.true(newStory.artefactLinks.length === 0, 'artefacts is now empty')

  ssb.close()
})

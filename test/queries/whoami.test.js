const tape = require('tape')
const { isFeed, isMsg, isCloakedMsgId: isGroup } = require('ssb-ref')
const TestBot = require('../test-bot')

tape('whoami', async (t) => {
  t.plan(5)

  const { ssb, apollo } = await TestBot({ loadContext: true })

  const result = await apollo.query({
    query: `{
      whoami { 
        public {
          feedId
          profileId
        }
        personal {
          groupId
          profileId
        }
      } 
    } `
  })

  // Ensure that basic query worked.
  t.error(result.errors, 'query should not return errors')

  const { feedId, profileId } = result.data.whoami.public
  t.true(isFeed(feedId), 'has feedId')
  t.true(isMsg(profileId), 'public profile (id)')

  const { groupId, profileId: _profileId } = result.data.whoami.personal
  t.true(isGroup(groupId), 'has feedId')
  t.true(isMsg(_profileId), 'personal profile (id)')

  ssb.close()
})

const test = require('tape')

const findTreeLinks = require('../src/find-tree-links')

function linkify (str) {
  const bits = str.split(/-+/)

  switch (bits.length) {
    case 2: return { parent: bits[0], child: bits[1] }
    case 3: return { parent: bits[0], relationshipType: bits[1], child: bits[2] }
    default: throw new Error('do not know how to process ' + str)
  }
}
function compare (A, B) {
  const keyA = [A.parent, A.child].join('-')
  const keyB = [B.parent, B.child].join('-')
  if (keyA > keyB) return 1
  if (keyA < keyB) return -1
  else return 0
}

test('findTreeLinks', t => {
  // warm up! (check our test helper is good)
  t.deepEqual(linkify('A-B'), { parent: 'A', child: 'B' }, 'linkify (simple)')
  t.deepEqual(
    linkify('A--birth--B'),
    { parent: 'A', child: 'B', relationshipType: 'birth' },
    'linkify (complex)'
  )
  console.log('---')

  const linksEqual = (listA, listB, msg) => {
    t.deepEqual(listA.sort(compare), listB.sort(compare), msg)
  }

  //
  //       v
  //   A---B   C---X
  //     |       |
  //    ab      xy
  //
  let childLinks = ['A-ab', 'B-ab', 'X-xy'].map(linkify)
  let partnerLinks = ['A-B', 'C-X'].map(linkify)

  let links = findTreeLinks('B', childLinks, partnerLinks)
  linksEqual(links.childLinks, ['A-ab', 'B-ab'].map(linkify), 'basic tree')
  linksEqual(links.partnerLinks, ['A-B'].map(linkify), 'ignores unconnected links')

  //
  //       v
  //   A---B
  //     |
  //    ab
  //   /  \
  //  1   2
  //
  childLinks = ['A-ab', 'B-ab', 'ab-1', 'ab-2'].map(linkify)
  partnerLinks = ['A-B'].map(linkify)

  links = findTreeLinks('B', childLinks, partnerLinks)
  linksEqual(
    links.childLinks,
    ['A-ab', 'B-ab', 'ab-1', 'ab-2'].map(linkify),
    'recurses through geneerations'
  )

  //
  //                   CC
  //           v       |
  //   Y   X   A---B   C
  //    \ / \ /  |  \ /
  //    xy  ax   ab  bc
  //
  childLinks = ['Y-xy', 'X-xy', 'X-ax', 'A-ax', 'A-ab', 'B-ab', 'B-bc', 'C-bc', 'CC-C'].map(linkify)
  partnerLinks = ['A-B'].map(linkify)

  links = findTreeLinks('A', childLinks, partnerLinks)
  linksEqual(
    links.childLinks,
    ['Y-xy', 'X-xy', 'X-ax', 'A-ax', 'A-ab', 'B-ab', 'B-bc', 'C-bc'].map(linkify),
    'pulls in extended family (and does not go up too high)'
  )
  linksEqual(links.partnerLinks, ['A-B'].map(linkify))

  //      v
  //  X∙∙∙A
  //   \ /
  //   ax
  //
  // NOTE X-A is an inferred link!
  // because ax is a child they are both related to by birth

  childLinks = ['X--birth--ax', 'A--birth--ax'].map(linkify)
  partnerLinks = []

  links = findTreeLinks('A', childLinks, partnerLinks)
  linksEqual(links.partnerLinks, ['A--inferred--X'].map(linkify), 'builds inferred partner links')

  t.end()
})

const tape = require('tape')
const { promisify: p } = require('util')
const TestBot = require('./test-bot')
const { SaveProfile, SaveLink, GetProfile, GetWhakapapaLink } = require('./lib/helpers')

tape('person.partners (saveLink, person, getWhakapapaLink)', async t => {
  t.plan(22)

  const { ssb, apollo } = await TestBot({ loadContext: true })
  const saveProfile = SaveProfile(apollo, t)
  const saveLink = SaveLink(apollo, t)
  const getProfile = GetProfile(apollo, t)
  const getWhakapapaLink = GetWhakapapaLink(apollo, t)

  const { groupId } = await p(ssb.tribes.create)({})
  const recps = [groupId]

  const requiredFields = {
    type: 'person',
    recps,
    authors: {
      add: [ssb.id]
    }
  }

  // CREATE TEST MAIN PARENT PROFILE
  const dad = await saveProfile({
    preferredName: 'Stacey',
    ...requiredFields
  })

  // CREATE TEST PARTNER 2 PROFILE
  const mum = await saveProfile({
    preferredName: 'Claudine',
    ...requiredFields
  })

  // CREATE ANOTHER partner
  const stepmum = await saveProfile({
    preferredName: 'Susan',
    ...requiredFields
  })

  // CREATE LINK BETWEEN THE DAD AND MUM
  const link1Id = await saveLink({
    type: 'link/profile-profile/partner',
    parent: dad,
    child: mum,
    recps
  })
  // CREATE A LINK FOR THE SECOND PARTNER (STEP MUM)
  const link2Id = await saveLink({
    type: 'link/profile-profile/partner',
    parent: dad,
    child: stepmum,
    recps
  })

  // GET LINK USING DAD
  const dadsProfile = await getProfile(dad)

  t.equal(dadsProfile.partners.length, 2, 'dads profile returns correct amount of partners and no duplicates')

  /* partner 1 = mum */
  t.equal(dadsProfile.partners[0].id, mum, 'id matches mums')

  /* child 2 */
  t.equal(dadsProfile.partners[1].id, stepmum, 'id matches step mums')

  // tombstone the link between mum and dad
  await saveLink({
    linkId: link1Id,
    tombstone: {
      date: Date.now(),
      reason: 'duplicate'
    }
  })

  await p(setTimeout)(1000)

  const updatedDadsProfile = await getProfile(dad)

  t.equal(updatedDadsProfile.id, dad, 'returned id matches dads id')
  t.equal(updatedDadsProfile.partners.length, 1, 'updated dads profile returns correct amount of partners')
  t.equal(updatedDadsProfile.partners[0].id, stepmum, 'id matches stepmums')

  // mum shouldnt have dad as a partner
  const mumsProfile = await getProfile(mum)
  t.deepEqual(mumsProfile.partners, [], 'mum has no partners')

  // stepmum should have dad as a partner
  const stepmumsProfile = await getProfile(stepmum)
  t.equal(stepmumsProfile.partners.length, 1, 'step mum has correct amount of partners')

  // get the link between two partners
  const link = await getWhakapapaLink(dad, stepmum, true)

  // expect the reverse to return the same
  const reverseLink = await getWhakapapaLink(stepmum, dad, true)

  t.deepEqual(link, reverseLink, 'reversing links returns the same for partners')

  t.deepEqual(
    link,
    {
      linkId: link2Id,
      type: 'link/profile-profile/partner',
      parent: dad,
      child: stepmum,
      relationshipType: null,
      legallyAdopted: null,
      tombstone: null,
      recps: [groupId]
    },
    'returns link between dad and stepmum'
  )

  ssb.close()
})
